package com.vblago.ui;

import com.vblago.util.Generator;
import com.vblago.util.PrintLit;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintLit printLit = new PrintLit(Generator.getLiter());
        printLit.printAll();
        System.out.println("Input sort by year(example: 1, 3 or 5): ");
        printLit.sort(Integer.parseInt(in.nextLine()));
    }
}
