package com.vblago.model;

public class Book extends Literature {
    private String author;

    public Book(String name, String publisher, int year, String author) {
        super(name, publisher);
        this.year = year;
        this.author = author;
    }

    @Override
    public void print() {
        System.out.println(name);
        System.out.println(publisher);
        System.out.println(author);
        System.out.println(year);
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", year=" + year +
                ", name='" + name + '\'' +
                ", publisher='" + publisher + '\'' +
                '}';
    }
}
