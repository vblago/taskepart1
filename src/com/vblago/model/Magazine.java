package com.vblago.model;

import java.util.Date;

public class Magazine extends Literature {
    private int articles;
    private Date date;

    public Magazine(String name, String publisher, int articles, Date date) {
        super(name, publisher);
        this.articles = articles;
        this.date = date;
        this.year = date.getYear();
    }

    @Override
    public void print() {
        System.out.println(name);
        System.out.println(publisher);
        System.out.println(articles);
        System.out.println(date.toString());
    }

    @Override
    public String toString() {
        return "Magazine{" +
                "articles=" + articles +
                ", date=" + date +
                ", name='" + name + '\'' +
                ", publisher='" + publisher + '\'' +
                '}';
    }
}
