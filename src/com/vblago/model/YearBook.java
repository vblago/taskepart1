package com.vblago.model;

public class YearBook extends Literature {
    private String about;

    public YearBook(String name, String publisher, int year, String about) {
        super(name, publisher);
        this.year = year;
        this.about = about;
    }

    @Override
    public void print() {
        System.out.println(name);
        System.out.println(publisher);
        System.out.println(about);
        System.out.println(year);
    }

    @Override
    public String toString() {
        return "YearBook{" +
                "about='" + about + '\'' +
                ", year=" + year +
                ", name='" + name + '\'' +
                ", publisher='" + publisher + '\'' +
                '}';
    }
}
