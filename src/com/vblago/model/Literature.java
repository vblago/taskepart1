package com.vblago.model;

public abstract class Literature {
    public String name;
    public String publisher;
    public int year;

    public Literature(String name, String publisher) {
        this.name = name;
        this.publisher = publisher;
    }

    public abstract void print();

    public boolean isSortTrue(int result) {
        return year >= result;
    }

    @Override
    public String toString() {
        return "Literature{" +
                "name='" + name + '\'' +
                ", publisher='" + publisher + '\'' +
                '}';
    }
}
