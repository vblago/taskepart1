package com.vblago.util;

import com.vblago.model.Book;
import com.vblago.model.Literature;
import com.vblago.model.Magazine;
import com.vblago.model.YearBook;

import java.util.Date;

public final class Generator {
    private static Literature[] liter;

    private Generator() {
    }

    public static Literature[] getLiter() {
        liter = new Literature[3];
        liter[0] = new Book("Книга", "Питер", 2015, "Шилд");
        liter[1] = new Magazine("Журнал", "Москва", 25, new Date(2011, 4, 22));
        liter[2] = new YearBook("Ежегодник", "Казань", 2014, "Конференции");
        return liter;
    }
}
