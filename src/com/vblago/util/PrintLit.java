package com.vblago.util;

import com.vblago.model.Literature;

public final class PrintLit {
    public Literature[] liter;

    public PrintLit(Literature[] liter) {
        this.liter = liter;
    }

    public void printAll() {
        for (Literature literature : liter) {
            //System.out.println(literature);
            literature.print();

        }
    }

    public void sort(int sortBy) {
        SortLit sortLit = new SortLit();
        Literature[] sortLiter = sortLit.sort(liter, sortBy);
        for (Literature literature : sortLiter) {
            System.out.println(literature);
        }
        if (sortLiter.length == 0) {
            System.out.println("All literature are older");
        }
    }

}
