package com.vblago.util;

import com.vblago.model.Literature;

public class SortLit {
    private static final int thisYear = 2017;

    public Literature[] sort(Literature[] fullLit, int sortBy) {
        Literature[] liter;
        int counter = 0;
        for (Literature literFromArr : fullLit) {
            if (literFromArr.isSortTrue(thisYear - sortBy)) {
                counter++;
            }
        }
        liter = new Literature[counter];
        counter = 0;
        for (Literature literFromArr : fullLit) {
            if (literFromArr.isSortTrue(thisYear - sortBy)) {
                liter[counter] = literFromArr;
                counter++;
            }
        }
        return liter;
    }
}
